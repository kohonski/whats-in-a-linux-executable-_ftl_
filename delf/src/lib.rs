mod parse;

extern crate nom;
extern crate derive_try_from_primitive;

use derive_try_from_primitive::TryFromPrimitive;

#[derive(Debug)]
pub struct File {
    pub r#type: Type,
    pub machine: Machine,
}

impl File {

    pub fn parse(i: parse::Input) -> parse::Result<Self> {
        use nom::{
            bytes::complete::{tag, take},
            error::context,
            sequence::tuple,
            combinator::map,
            number::complete::le_u16,
        };

        let (i, (r#type, machine)) = tuple((
            context("Type", map(le_u16, |x| Type::try_from(x).unwrap())),
            context("Machine", map(le_u16, |x| Machine::try_from(x).unwrap())),
        ))(i)?;

        let res = Self { machine, r#type };
        Ok((i, res))
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, TryFromPrimitive)]
#[repr(u16)]
pub enum Type {
    None = 0x0,
    Rel = 0x1,
    Exec = 0x2,
    Dyn = 0x3,
    Core = 0x4,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, TryFromPrimitive)]
#[repr(u16)]
pub enum Machine {
    X86 = 0x03,
    X86_64 = 0x3e,
}

pub struct HexDump<'a>(&'a [u8]);
use std::fmt;

impl<'a> fmt::Debug for HexDump<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for &x in self.0.iter().take(20) {
            write!(f, "{:02x} ", x)?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::{Type, Machine};
    

    #[test]
    fn type_to_u16(){
        assert_eq!(Type::Dyn as u16, 0x3)
    }

    #[test]
    fn try_enums() {
        assert_eq!(Type::try_from(0x3), Some(Type::Dyn));
        assert_eq!(Machine::X86 as u16, 0x03);
        assert_eq!(Machine::X86_64 as u16, 0x3e);
        assert_eq!(Machine::try_from(0x03), Some(Machine::X86));
        assert_eq!(Machine::try_from(0x3e), Some(Machine::X86_64));
        assert_eq!(Machine::try_from(0xff), None)
    }
}
