### Setup
1. Run `cd elk && cargo build --all`

### Produce issue
1. Ensure you're still in the elk directory
2. Run `./target/debug/elk /bin/true`

Expected output:
```sh
thread 'main' panicked at 'called `Option::unwrap()` on a `None` value', /path/to/whats-in-a-linux-executable/delf/src/lib.rs:28:45
note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace
```

We get this line from the parsing function:
```rust
        let (i, (r#type, machine)) = tuple((
            context("Type", map(le_u16, |x| Type::try_from(x).unwrap())),
            context("Machine", map(le_u16, |x| Machine::try_from(x).unwrap())),
        ))(i)?;
```

These are our two types:
```rust
#[derive(Debug, Clone, Copy, PartialEq, Eq, TryFromPrimitive)]
#[repr(u16)]
pub enum Type {
    None = 0x0,
    Rel = 0x1,
    Exec = 0x2,
    Dyn = 0x3,
    Core = 0x4,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, TryFromPrimitive)]
#[repr(u16)]
pub enum Machine {
    X86 = 0x03,
    X86_64 = 0x3e,
}
```

### In summary
This is what Amos has in [his blog](https://fasterthanli.me/blog/2020/whats-in-a-linux-executable/).

His expected output is: 
```sh
$ # in elk/
$ cargo b -q
$ ./target/debug/elk /bin/true
File {
    type: Dyn,
    machine: X86_64,
}
```

I just can't figure out where the error could be coming from? Is it that I'm importing try_from_primitive wrong?

Are my cargo crates not set up correctly? I understand that unwrap() will panic by default if an option doesn't exist. But what could I be missing that amos isn't?

